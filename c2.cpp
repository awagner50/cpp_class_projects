//Alex Wagner CIT-245-Z01
//Homework Chapter 2 9/10/22

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>

int main()
{
	using namespace std;

	//Variables
	double radius, weight, volume, buoyancy;
	int prompt;
	const double PI = 3.141592;

	cout << "This program will determine if a shpere will sink or float in the water.\n";

	//Loop asks user for radius and weight. It calculates the buoyancy to see if it will float or sink. 
	//Shows results. Then asks user if they want to repeat.
	do
	{
		cout << "\nEnter the radius of the sphere: ";
		cin  >> radius;
		cout << "Enter the weight of the sphere: ";
		cin  >> weight;

		//Calculations
		volume	 = (4.0/3.0) * PI * (radius * radius * radius);
		buoyancy = volume * 62.4;

		cout << "\nThe Buoyant Force = " << buoyancy;

		if (weight <= buoyancy) 
			cout << "\nNice, your sphere floats.\n";
		else 
			cout << "\nOh no, your sphere sank down into the water.\n";

		//Asks for reprompt.
		cout << "\nWould you like to make another calculation?\n" 
			 << "(Type any number to continue or 0 to EXIT) --> ";
		cin  >> prompt;

	} while (prompt != 0);

	system("pause");
	return 0;
}