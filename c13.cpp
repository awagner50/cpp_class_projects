//Alex Wagner CIT-245-Z01
//Homework Chapter 13 10/29/22
//Tower of Hanoi with recursion

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>

void ToH(int n, char s, char t, char g);

int main()
{
	using std::cout;
	using std::cin;

	char prompt;
	double numOfDiscs;

	do {
		cout << "~~~ Tower of Hanoi ~~~\n";
		do {
			cout << "Enter a number of discs (at least 3): ";
			cin >> numOfDiscs;
		} while (numOfDiscs < 3);

		cout << "start rod is 1 | goal rod is 2 | temporary rod is 3\n";
		ToH(numOfDiscs, '1', '3', '2');

		cout << "2 to the power " << numOfDiscs << " power = " << pow(2.0, numOfDiscs) << "\n";
		cout << "Number of moves " << (pow(2.0, numOfDiscs) - 1) << "\n";

		//Prompt user for retry.
		cout << "\nType \"Y\" to try again or \"N\" to stop.\n";
		cin >> prompt;
		cout << "\n";

	} while (prompt == 'y' || prompt == 'Y');
}

void ToH(int n, char sRod, char tRod, char gRod)
{
	using std::cout;

	if (n == 1)
	{
		cout << "From " << sRod << " to " << gRod << "\n";
	}
	else
	{
		ToH(n - 1, sRod, gRod, tRod);
		cout << "From " << sRod << " to " << gRod << "\n";
		ToH(n - 1, tRod, sRod, gRod);
	}
}