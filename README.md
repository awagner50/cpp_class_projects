# Data Structure/Algorithms C++: Alex's student repository

This repository holds C++ code that was created for my classes from CCAC. (Spring 2022)
This class mostly dealt creating basic webpages using html and css.
All assignments and tests covered here will be from this class (CIT-245).

# Assignemnt 1: Babylons algorithm

This assignment is very basic. It contains a c++ file. Starting off slow with the basics.

# Assignemnt 2: Buoyancy calculator

This assignment contains a c++ file. Loop asks user for radius and weight. 
It calculates the buoyancy to see if it will float or sink. 

# Assignemnt 3: Dice rolling and probability

This assignment contains a c++ file. Promts user how many rolls they want, The higher the number the better the results.
Rolls dice that many time and determines the odds of all the rolls.

# Assignemnt 4: Greatest Common Denominator and simplifies fraction

This assignment contains a c++ file. Prompts user for a numerator and denominator. 
Then, the program finds the gcd and simplies the fraction.

# Assignemnt 5: Random numbers

This assignment contains a c++ fGernerates 20 random numbers, puts them in a list, sorts the lists, and then ile. displayes the occurnece of each number.

# Assignemnt 6: Functions

This assignment contains a c++ file. Creates many functions to generate three video game characters.

# Assignemnt 7: Classes

This assignment contains a c++ file. Introductions to classes. Increments and decrements.

# Assignemnt 8: Indepth classes

This assignment contains a c++ file. Uses constructors and overloads operators. Uses public and private classes.

# Assignemnt 9: String reversal

This assignment contains a c++ file. Prompts user to enter a string then the program will reverse it.

# Assignemnt 10: Pointers, dynamic array, copy constructors and a wrapper class

This assignment contains a c++ file. This program contains 2 parts.

# Assignemnt 11: Call functions from another file from the main file

This assignment contains 5 files. 3 cpp and 2 h files. You would run the Driver.cpp file.

# Assignemnt 12: Read and write to a txt file

This contains 2 files, a txt and cpp.

# Assignemnt 13: Tower of Hanoi with recursion

This assignment contains a c++ file. this program is about solving the tower of hanoi with recursion.

# Assignemnt 14: Displays a gun cabinet

This assignment contains 9 files. 5 cpp and 4 h files. You would run the gunDemo.cpp file.

# Assignemnt 16: Template Binary search

This assignment contains a c++ file. This program is recursive binary search function.

# Assignemnt 17: Singly and doubly linked lists

This assignment contains a c++ file. The assignment had me enhamce the professors program to creat doubly linked list. The professor provied the singly linked list.

# Assignemnt 19: Mapping

This assignment contains a c++ file. Uses c++ map to store social security numbers by names.
