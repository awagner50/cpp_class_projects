//Alex Wagner CIT-245-Z01
//Homework Chapter 4 9/17/22

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>
using namespace std;

//gcd function uses the numerator and denominator to find the Greatest Common Denominator.
int gcd(int num, int den);
//reduce function calls the gcd and uses it simplify the given fraction.
void reduce(int& num, int& den);

int main()
{
	int num, den;
	char prompt;

	cout << "I will simplify your fraction:\n";

	//Loop repeats with user input. Gets num and den then calls reduce function.
	do
	{	
		cout << "Enter the numerator: ";
		cin >> num;
		cout << "Enter the denominator: ";
		cin >> den;

		reduce(num, den);

		cout << "Your simplified fraction is " << num << "/" << den << "\n";

		//Prompt user for retry.
		cout << "Type \"Y\" to try again or \"N\" to stop.\n";
		cin >> prompt;
		cout << "\n";

	} while (prompt == 'y' || prompt == 'Y');

	return 0;
}

int gcd(int num, int den)
{
	int result = NULL;

	for (int i = 1; i <= num; ++i)
	{
		if (den % i == 0 && num % i == 0)
		{
			result = i;
		}
	}

	return result;
}

void reduce(int& num, int& den)
{
	int g = gcd(num, den);
	cout << "The greatest common denominator is " << g << "\n";

	num = num / g;
	den = den / g;
}