//Alex Wagner CIT-245-Z01
//Homework Chapter 5 9/24/22

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>
#include<algorithm>
using namespace std;

int main()
{
	//variables for switch case and countArray
	int c1, c2, c3, c4, c5, c6, c7, c8, c9, c10;
	char prompt;

	cout << "Program will generate 20 random numbers, sort them, and then count each occurrence.\n";


	do
	{
		//Refreshing values at zero.
		c1 = 0, c2 = 0, c3 = 0, c4 = 0, c5 = 0, c6 = 0, c7 = 0, c8 = 0, c9 = 0, c10 = 0;

		//Filling the array with random #'s
		int numArray[20];

		srand(time(NULL));
		for (int i = 0; i < 20; i++)
		{
			numArray[i] = rand() % 10 + 1;
		}


		//Sorting the array
		sort(numArray, numArray + 20);


		//Printing sorted array & counting each instance of a number
		cout << "\nSorted list of random numbers:\n";

		for (size_t i = 0; i < 20; i++)
		{
			cout << numArray[i] << " ";

			switch(numArray[i])
			{
				case 1:
					c1++;
					break;
				case 2:
					c2++;
					break;
				case 3:
					c3++;
					break;
				case 4:
					c4++;
					break;
				case 5:
					c5++;
					break;
				case 6:
					c6++;
					break;
				case 7:
					c7++;
					break;
				case 8:
					c8++;
					break;
				case 9:
					c9++;
					break;
				case 10:
					c10++;
					break;
			}
		}


		//Made an array with the totaled occurrences then printed them all
		int countArray[10] = {c1, c2, c3, c4, c5, c6, c7, c8, c9, c10};

		cout << "\n";
		cout << "\nCount of each occurrence:\n";

		for (size_t i = 0; i < 10; i++)
		{
			cout << i+1 << ":	" << countArray[i] << "\n";
		}


		//Prompt user for retry.
		cout << "\nType \"Y\" to try again or \"N\" to stop.\n";
		cin >> prompt;
		cout << "\n";

	} while (prompt == 'y' || prompt == 'Y');
	
}