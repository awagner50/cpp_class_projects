//Alex Wagner CIT-245-Z01
//Homework Chapter 9 10/8/22

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>
#include <stack>

using namespace std;

//fuction to reverse the the given char array
void flip(char arr[]);

int main()
{
    cout << "I will reverse the string you enter using a stack.\n";
	char prompt;

    //allocates memory
    char* s = (char*)malloc(80);

	do
	{
		cout << "Enter a string less than 80 characters followed by an ENTER key\n";
		cin.getline(s, 81, '\n');
        
        flip(s);
        cout << s << "\n";

		//Prompt user for retry.
		cout << "\nType \"Y\" to flip a differnt string or \"N\" to stop.\n";
        cin >> prompt;
        cout << "\n";

        //creates a buffer so user can repeat
        int c;
        while ((c = getchar()) != '\n' && c != EOF);

	} while (prompt == 'y' || prompt == 'Y');

    //gets rid of memory allocated wih malloc
    free(s);
}

void flip(char a[])
{
    stack<char> stack;
    int n = strlen(a);

    //pushing elements into stack
    for (int i = 0; i < n; i++) 
    {
        stack.push(a[i]);
    }

    int x = 0; //index

    //popping values from stack until empty
    while (!stack.empty())
    {
        a[x++] = stack.top();
        stack.pop();
    }
}