//Alex Wagner CIT-245-Z01
//Homework Chapter 16 11/12/22
//Template Binary search

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>

using namespace std;

//recursive binary search function
template<class T>
void search(T a[], int first, int last,
	T key, bool& found, int& location)
{
	int mid;
	if (first > last)
	{
		found = false;
	}
	else
	{
		mid = (first + last) / 2;
		/*key is the variable that search searches for in the given array.
		location is the idexed spot where key is found.*/
		if (key == a[mid])
		{
			found = true;
			location = mid;
		}
		else if (key < a[mid])
		{
			search(a, first, mid - 1, key, found, location);
		}
		else if (key > a[mid])
		{
			search(a, mid + 1, last, key, found, location);
		}
	}
}

int main()
{
	int iarr[13] = {0, 1, 1, 2, 5, 7, 8, 11, 11, 11, 13, 17, 18};
	int finalIndex = 13-1;//last index
	int location;//index in which the key is found inside the array
	bool found;//in array/not in array
	cout << "Integer test array contains:\n";
	for (size_t i = 0; i < 13; i++)//prints int array
	{
		cout << iarr[i] << " ";
	}
	cout << "\n";
	//goes -1 to 18. each time calling search to search through the array for i
	for (int i = -1; i < 19; i++)
	{
		search(iarr, 0, finalIndex, i, found, location);
		if (found)
			cout << i << " is at index " << location << "\n";
		else
			cout << i << " is not in the array." << "\n";
	}
	//Char array section stars
	char carr[11] = {'a', 'c', 'd', 'h', 'i', 'i', 'm', 'r', 'w', 'w', 'x'};
	finalIndex = 11-1;
	cout << "\nChar test array contains:\n";
	for (size_t i = 0; i < 11; i++)//prints char array
	{
		cout << carr[i] << " ";
	}
	cout << "\n";
	//goes a to z. each time calling search to search through the array for i
	for (char i = 'a'; i < '{'; i++)
	{
		search(carr, 0, finalIndex, i, found, location);
		if (found)
			cout << i << " is at index " << location << "\n";
		else
			cout << i << " is not in the array." << "\n";
	}
	return 0;
}