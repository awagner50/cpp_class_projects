//Alex Wagner CIT-245-Z01
//Homework Chapter 1 11/10/22

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>

int main()
{
	using namespace std;

	//Variables
	double guess, input, r;

	//Asks for user input of any integer then gets that user input.
	cout << "The program will use the Babylonian square root algorithm until it's .001 within correct answer.\n"
		 << "Enter an integer: ";
	cin  >> input;
	cout << "You entered: " << input << "\n" << "\n";

	guess = input / 2;

	//This loop repeats until guess is at least .001 decimal points away from the square root of input.
	while (guess - sqrt(input) > .001)
	{
		r     = input / guess;
		guess = (guess + r) / 2;

		cout << "Guessed --> " << guess << "\n";	 
	}

	//Outputs answer and shows its accuracy.
	cout << "\nThe Babylons algorithm response is --> " << guess << "\n"
		 << "Checking accuracy of program: " << guess << " * " << guess << " = " << (guess * guess) << "\n";

	system("pause");
	return 0;
}