/*
 Alex Wagner CIT-245-Z01
 Homework Chapter 16 11/19/22
 Written singly linked list by Professor Kenneth L Moore
 Enhanced/changed to a doubly linked list by Alex
*/
#include <iostream>
#include <string>
using namespace std;
// define a node for storage and linking
class node {
public:
	string name;
	node* next;
	node *prev;
};
class linkedList {
public:
	linkedList() :top(NULL), end(NULL) {}
	bool empty() { return top == NULL; }
	node* getTop() { return top; } 
	void setTop(node* n) { top = n; }
	node* getEnd() { return end; }
	void setEnd(node* e) { end = e; }
	void add(string);
	int menu();
	void remove(string);
	~linkedList();
	void reversePrint();
	friend ostream& operator << (ostream&, const linkedList&); // default output is in-order print.
private:
	node* top;
	node* end;
};
int main() {
	linkedList l;
	cout << l.empty() << endl;
	int option = 0;
	string s;
	bool go = true;
	while (go) {
		option = l.menu();
		switch (option) {
		case 1: cout << "enter a name: "; cin >> s; l.add(s); break;
		case 2: cout << "enter name to be deleted: "; cin >> s; l.remove(s); break;
		case 3: cout << l; break;
		case 4: l.reversePrint(); break;
		case 5: cout << "exiting" << endl; go = false; break;
		}
	}
	// l goes out of scope and calls ~linkedList()
}
// can not call this method "delete" - "delete" is a reserved keyword.
void linkedList::remove(string s) {
	bool found = false;
	node* curr = getTop(), * prev = NULL;
	while (curr != NULL) {
		// if match found, delete
		if (curr->name == s) {
			found = true;
			// if 1 node in list
			if (getTop() == getEnd()) {
				top = NULL;
				end = NULL;
			}
			// if found at top
			else if (curr->prev == NULL) {
				setTop(curr->next);
				curr->next->prev = NULL;
			}
			// found in list - not top
			else {
				// if last node is deleted node
				if (curr->next == NULL) {
					setEnd(curr->prev);
					curr->prev->next = curr->next;
				}
				// if middle node is deleted node
				else {
					curr->prev->next = curr->next;
					curr->next->prev = curr->prev;
				}
			}
			delete(curr);
		}
		// not found, advance pointers
		if (!found) {
			prev = curr;
			curr = curr->next;
		}
		// found, exit loop
		else curr = NULL;
	}
	if (found)cout << "Deleted " << s << endl;
	else cout << s << " Not Found " << endl;
}
void linkedList::add(string s) {
	node* n = new node();
	n->name = s;
	n->next = NULL;
	n->prev = NULL;
	// takes care of empty list case
	if (empty()) {
		top = n;
		end = n;
	}
	// takes care of node belonging at beginning case
	else if (getTop()->name > s) {
		n->next = getTop();
		getTop()->prev = n;
		setTop(n);
	}
	// takes care of inorder and end insert
	else {
		// insert in order case
		node* curr = getTop();
		while (curr != NULL) {
			if (curr->name > s)break;
			curr = curr->next;
		}
		if (curr != NULL) { // search found insert point
			n->next = curr;
			curr->prev->next = n;
			n->prev = curr->prev;
			curr->prev = n;
		}
		// take care of end of list insertion
		else if (curr == NULL) {// search did not find insert point
			getEnd()->next = n;
			n->prev = getEnd();
			setEnd(getEnd()->next);
		}
	}
}
ostream& operator << (ostream& os, const linkedList& ll) {
	//linkedList x = ll; // put this in and the code blows up - why?
	node* n = ll.top;
	if (n == NULL)cout << "List is empty." << endl;
	else
		while (n != NULL) {
			os << n->name << endl;
			n = n->next;
		}
	return os;
}
// return memory to heap
linkedList::~linkedList() {
	cout << "~linkedList called." << endl;
	node* curr = getTop(), * del;
	while (curr != NULL) {
		del = curr;
		curr = curr->next;
		delete(del);
	}
}
int linkedList::menu() {
	int choice = 0;
	while (choice < 1 || choice > 5) {
		cout << "\nEnter your choice" << endl;
		cout << " 1. Add a name." << endl;
		cout << " 2. Delete a name." << endl;
		cout << " 3. Show list." << endl;
		cout << " 4. Show reverse list. " << endl;
		cout << " 5. EXIT " << endl;
		cin >> choice;
	}
	return choice;
}
// print the linked list in reverse order
void linkedList::reversePrint() {
	node* n = getEnd();
	if (n == NULL)cout << "List is empty." << endl;
	else
		while (n != NULL) {
			cout << n->name << endl;
			n = n->prev;
		}
}