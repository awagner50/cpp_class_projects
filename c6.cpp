//Alex Wagner CIT-245-Z01
//Homework Chapter 6 9/24/22

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>

using namespace std;

class Player
{
public:
	void display();
	//mutators
	void setName(string n);
	void setPword(string pw);
	void setExp(int e);
	void setLoc(int x, int y);
	void setInv(string i[4]);

	//accessors
	string getName();
	string getPword();
	int getExp();
	int getLoc();
	string getInv();

private:
	string name;
	string pword;
	int exp;
	int coorX;
	int coorY;
	string Inv[4];
};

int main()
{
	cout << "Here are 3 generated players.\n";

	Player p1, p2, p3;
	
	//Player 1
	p1.setName("Arthur Morgan");
	p1.setPword("aSkfDgbQ");
	p1.setExp(74356);
	p1.setLoc(-278, 807);
	string p1Items[4] = { " Schofield Revolver", " Bandana", " Cowboy Hat", " Journal" };
	p1.setInv(p1Items);
	p1.display();

	//Player 2
	p2.setName("Sadie Adler");
	p2.setPword("ALaWqzxI");
	p2.setExp(27983);
	p2.setLoc(-798, -1247);
	string p2Items[4] = { " Two Cattleman Revolvers", " Harmonica", " Neckerchief", " Cigarettes" };
	p2.setInv(p2Items);
	p2.display();

	//Player 3
	p2.setName("Dutch van der Linde");
	p2.setPword("KaUdfBQe");
	p2.setExp(81137);
	p2.setLoc(1997, -4499);
	string p3Items[4] = { " Two Double-action Revolvers", " Hankerchief", " Pocket Chain", " Cigars" };
	p2.setInv(p3Items);
	p2.display();
}

void Player::display()
{
	cout << "\n--- Player ---\n" << "Name: " << name;
	cout << "\nPassword: " << pword;
	cout << "\nExperience: " << exp;
	cout << "\nLocation: " << coorX << ", " << coorY;
	cout << "\nInventory:\n";
	for (int i = 0; i < 4; i++)
		cout << Inv[i] << "\n";

}

void Player::setName(string n)
{
	name = n;
}

string Player::getName()
{
	return name;
}

void Player::setPword(string pw)
{
	pword = pw;
}

string Player::getPword()
{
	return pword;
}

void Player::setExp(int e)
{
	exp = e;
}

int Player::getExp()
{
	return exp;
}

void Player::setLoc(int x, int y)
{
	coorX = x;
	coorY = y;
}

int Player::getLoc()
{
	return coorX;
	return coorY;
}

void Player::setInv(string i[4])
{
	for (int j = 0; j < 4; j++)
	{
		Inv[j] = i[j];
	}
}

string Player::getInv()
{
	return Inv[0];
}