//Alex Wagner CIT-245-Z01
//Homework Chapter 8 10/1/22

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

class Money
	{
public:
	//Contructors
	Money();
	Money(double a); //a = amount
	Money(int dol, int c); //dol = dollars, c = cents
	Money(int dol);

	//Getters
	double getAmount() const { return (dollars + cents * 0.01); }
	int getDollars() const { return dollars; }
	int getCents() const { return cents; }

	//Overloaded << and >> operators as friends
	friend ostream& operator <<(ostream& output, const Money& a);
	friend istream& operator >>(istream& input, Money& a);
	//Overloaded * operator as friend
	friend const Money operator *(const Money& a1, double num)
		{ return (a1.getAmount() * num); }
	//Overloaded + and - operators
	const Money operator +(const Money& a2) const;
	const Money operator -(const Money& a2) const;

private:
	int dollars;
	int cents;
	int dPart(double a) const;
	int cPart(double a) const;
	int round(double num) const;
	};

//Overloaded < and > operators
bool operator <(const Money& a1, const Money& a2) 
	{ return (a1.getAmount() < a2.getAmount()); }
bool operator >(const Money& a1, const Money& a2) 
	{ return (a1.getAmount() > a2.getAmount()); }
//Overloaded unary operator to make a negatve
const Money operator -(const Money& a) 
	{ return Money(-a.getDollars(), -a.getCents()); }

int main()
{
	Money yourAmount, myAmount(10, 9);

	cout << "Enter your amount of money: $";
	cin >> yourAmount;
	cout << "Your amount is " << yourAmount << '\n';
	cout << "My amount is " << myAmount << '\n';

	if (yourAmount < myAmount)
		cout << "One of us is richer than the other.\n"
		<< "I have more money than you.\n";
	else if (yourAmount > myAmount)
		cout << "One of us is richer than the other.\n"
		<< "You have more money than me.\n";
	else
		cout << "We have the same amount of money.\n";
		
	cout << "10% of your money is: " << yourAmount * .1 << "\n";

	Money sumAmount = yourAmount + myAmount;
	cout << yourAmount << " + " << myAmount << " equals " << sumAmount << "\n";
	Money difAmount = yourAmount - myAmount;
	cout << yourAmount << " - " << myAmount << " equals " << difAmount << "\n";
}

Money::Money() : dollars(0), cents(0)
{ }

Money::Money(double a) : dollars(dPart(a)), cents(cPart(a))
{ }

Money::Money(int dol, int c)
{
	if ((dol < 0 && c > 0) ||
		(dol > 0 && c < 0))
	{
		cout << "Inconsistent money data.\n";
		exit(1);
	}
	dollars = dol;
	cents = c;
}

Money::Money(int dol) : dollars(dol), cents(0)
{ }

ostream& operator <<(ostream& output, const Money& a)
{
	int absDol = abs(a.dollars);
	int absCents = abs(a.cents);
	if (a.dollars < 0 || a.cents < 0)
		//accounts for dollars == 0 or cents == 0
		output << "$-";
	else
		output << '$';
	output << absDol;
	if (absCents >= 10)
		output << '.' << absCents;
	else
		output << '.' << '0' << absCents;
	return output;
}

istream& operator >>(istream& input, Money& a)
{
	double amountAsDouble;
	input >> amountAsDouble;
	a.dollars = a.dPart(amountAsDouble);
	a.cents = a.cPart(amountAsDouble);
	return input;
	}

const Money Money:: operator +(const Money& secOperand) const
{
	int sumA1 = cents + dollars * 100;
	int sumA2 = secOperand.cents + secOperand.dollars * 100;
	int sumAllCents = sumA1 + sumA2;
	int absAllCents = abs(sumAllCents); //Money can be negative.
	int finalDol = absAllCents / 100;
	int finalCents = absAllCents % 100;

	if (sumAllCents < 0)
		{
		finalDol = -finalDol;
		finalCents = -finalCents;
		}
	return Money(finalDol, finalCents);
}

const Money Money:: operator -(const Money& secOperand) const
{
	int sumA1 = cents + dollars * 100;
	int sumA2 = secOperand.cents + secOperand.dollars * 100;
	int diffAllCents = sumA1 - sumA2;
	int absAllCents = abs(diffAllCents);
	int finalDol = absAllCents / 100;
	int finalCents = absAllCents % 100;

	if (diffAllCents < 0)
		{
		finalDol = -finalDol;
		finalCents = -finalCents;
	}
	return Money(finalDol, finalCents);
}

int Money::dPart(double a) const
{
	return static_cast <int>(a);
}

int Money::cPart(double a) const
{
	double doubleCents = a * 100;
	int intCents = (round(fabs(doubleCents))) % 100;
	if (a < 0)
		intCents = -intCents;
	return intCents;
}

int Money::round(double num) const
{
	return static_cast <int>(floor(num + 0.5));
}