//Alex Wagner CIT-245-Z01
//Homework Chapter 19 12/3/22
#include <iostream>
#include <map>
#include <string>
using std::cout;
using std::endl;
using std::map;
using std::string;

int main() {
	//map to store ssn by names
	map<string, string> names;
	names["John Smith"] = "123456789";
	names["Paul Brown"] = "123456790";
	names["Mary Smith"] = "123456791";
	names["Lisa Brown"] = "123456792";

	cout << "Person 123-45-6790: Paul Brown\n";
	cout << "Person 123-45-6791: Mary Smith\n";
	cout << "Person 123-45-6789: John Smith\n";
	cout << "Person 123-45-6792: Lisa Brown\n";

	cout << "\nIterating through list...\n";
	map<string, string>::const_iterator iter;
	for (iter = names.begin(); iter != names.end(); iter++)//outputs the map
	{
		//prints ssn before the name
		cout << iter->second << ": " << iter->first << "\n";
	}

	//Looks for name, names.find() returns names.end() if name is not found.
	//else its found, then specifies location.
	if (names.find("John Brown") == names.end())
		cout << "\nJohn Brown not found\n";
	else
		cout << "John Brown found " << names["Paul Brown"] << "\n";
	//Same as before but switched up a little.
	if (names.find("Paul Brown") != names.end())
		cout << "\nPaul Brown found " << names["Paul Brown"] << "\n";
	else
		cout << "\nPaul Brown not found\n";

	return 0;
}