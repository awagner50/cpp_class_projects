//Alex Wagner CIT-245-Z01
//Homework Chapter 7 10/1/22

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>

using namespace std;

class Counter
{
public:
	Counter();
	//^default constructor -- sets the count to zero
	Counter(int i);
	//^overloaded constructor -- initialize to any int
	void display();
	void inc();
	void dec();

	int getCount() const { return count; }

	static int getTotalIncDec() { return totalIncDec; }

private:
	int count;
	static int totalIncDec;
};

int Counter::totalIncDec = 0;

int main()
{
	Counter c1, c2(200);

	cout << "Current state of c1 (created by the default constructor)\n";
	c1.display();
	cout << "\nCurrent state of c2 (created by c2(200))\n";
	c2.display();

	//Increments c1 85 times
	for (int i = 0; i < 85; i++)
	{
		c1.inc();
	}
	cout << "\nCurrent state of c1 after 85 inc()\n";
	c1.display();

	//decrements c2 242 times.
	for (int i = 0; i < 242; i++)
	{
		c2.dec();
	}
	cout << "\nCurrent state of c2 after 242 dec()\n";
	c2.display();

}

Counter::Counter() : count(0)
{ }

Counter::Counter(int i) : count(i)
{ }

void Counter::display()
{
	cout << "Current count: " << count;
	cout << "\nTotal increments and decrements: " << totalIncDec << "\n";
}

void Counter::inc()
{
	count++;
	totalIncDec++;
}

void Counter::dec()
{
	count--;
	totalIncDec++;
}