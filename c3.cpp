//Alex Wagner CIT-245-Z01
//Homework Chapter 3 9/17/22

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>
using namespace std;

int main()
{
	//Some Variables
	char prompt;
	double totalRolls;
	double sum2, sum3, sum4, sum5, sum6, sum7, sum8, sum9, sum10, sum11, sum12;
	double odd1, odd2, odd3, odd4, odd5, odd6;

	//do-while loop that repeats when user inputs Y.
	do
	{
		//Prompt user for num of rolls.
		cout << "Enter the number of rolls you want: ";
		cin >> totalRolls;

		//Resets the sum count to 0.
		sum2 = 0.0, sum3 = 0.0, sum4 = 0.0, sum5 = 0.0, sum6 = 0.0, sum7 = 0.0, sum8 = 0.0, sum9 = 0.0, sum10 = 0.0, sum11 = 0.0, sum12 = 0.0;

		//For loop that makes 2 random dice rolls for given a amount of times. Then, counts the sum of the die each roll with a switch statement.
		srand(time(NULL));
		for (size_t i = 0; i < totalRolls; i++)
		{
			int die1, die2, dieSum;

			die1 = rand() % 6 + 1;
			die2 = rand() % 6 + 1;

			dieSum = die1 + die2;

			switch(dieSum)
			{
				case 2:
					sum2++;
					break;
				case 3:
					sum3++;
					break;
				case 4:
					sum4++;
					break;
				case 5:
					sum5++;
					break;
				case 6:
					sum6++;
					break;
				case 7:
					sum7++;
					break;
				case 8:
					sum8++;
					break;
				case 9:
					sum9++;
					break;
				case 10:
					sum10++;
					break;
				case 11:
					sum11++;
					break;
				case 12:
					sum12++;
					break;
			}
		}
		
		//Calculates the expected outcome of the dice rolls.
		odd1 = (1.0 / 36.0) * totalRolls;
		odd2 = (2.0 / 36.0) * totalRolls;
		odd3 = (3.0 / 36.0) * totalRolls;
		odd4 = (4.0 / 36.0) * totalRolls;
		odd5 = (5.0 / 36.0) * totalRolls;
		odd6 = (6.0 / 36.0) * totalRolls;

		//Displays results
		cout << "Sum of  2 was rolled " << sum2  << " times. The Odds were " << odd1 << ", so the %Error is " << abs((1.0 - (sum2  / odd1)) * 100.0) << "\n";
		cout << "Sum of  3 was rolled " << sum3  << " times. The Odds were " << odd2 << ", so the %Error is " << abs((1.0 - (sum3  / odd2)) * 100.0) << "\n";
		cout << "Sum of  4 was rolled " << sum4  << " times. The Odds were " << odd3 << ", so the %Error is " << abs((1.0 - (sum4  / odd3)) * 100.0) << "\n";
		cout << "Sum of  5 was rolled " << sum5  << " times. The Odds were " << odd4 << ", so the %Error is " << abs((1.0 - (sum5  / odd4)) * 100.0) << "\n";
		cout << "Sum of  6 was rolled " << sum6  << " times. The Odds were " << odd5 << ", so the %Error is " << abs((1.0 - (sum6  / odd5)) * 100.0) << "\n";
		cout << "Sum of  7 was rolled " << sum7  << " times. The Odds were " << odd6 << ", so the %Error is " << abs((1.0 - (sum7  / odd6)) * 100.0) << "\n";
		cout << "Sum of  8 was rolled " << sum8  << " times. The Odds were " << odd5 << ", so the %Error is " << abs((1.0 - (sum8  / odd5)) * 100.0) << "\n";
		cout << "Sum of  9 was rolled " << sum9  << " times. The Odds were " << odd4 << ", so the %Error is " << abs((1.0 - (sum9  / odd4)) * 100.0) << "\n";
		cout << "Sum of 10 was rolled " << sum10 << " times. The Odds were " << odd3 << ", so the %Error is " << abs((1.0 - (sum10 / odd3)) * 100.0) << "\n";
		cout << "Sum of 11 was rolled " << sum11 << " times. The Odds were " << odd2 << ", so the %Error is " << abs((1.0 - (sum11 / odd2)) * 100.0) << "\n";
		cout << "Sum of 12 was rolled " << sum12 << " times. The Odds were " << odd1 << ", so the %Error is " << abs((1.0 - (sum12 / odd1)) * 100.0) << "\n";

		//Prompt user for retry.
		cout << "Type \"Y\" to try again or \"N\" to stop.\n";
		cin >> prompt;
		cout << "\n";

	} while (prompt == 'y' || prompt == 'Y');

	return 0;
}