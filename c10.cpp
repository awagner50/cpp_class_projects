//Alex Wagner CIT-245-Z01
//Homework Chapter 10 10/15/22

#pragma warning(disable: 4996)
#include<string>
#include<stdlib.h>
#include<time.h>
#include <iostream>

using namespace std;

//Deep Copy Class
class WrapArrayDeep 
{
public:
	void display(); //prints the array
	void change(); //changes the array
	//default contructor
	WrapArrayDeep() 
	{
		pch = new char[5];
		*pch = 97;
		*(pch + 1) = 98;
		*(pch + 2) = 99;
		*(pch + 3) = 100;
		*(pch + 4) = 101;
	}
	//copy constructor
	WrapArrayDeep(const WrapArrayDeep& wad) 
	{
		pch = new char[5];
		for (int i = 0; i < 5; i++)
			*(pch + i) = wad.pch[i];
	}
	//destructor
	~WrapArrayDeep() 
	{
		cout << "calling destructor for WrapArrayDeep\n";
		delete[] pch;
	}
private:
		char* pch;
};

//Shallow Copy Class
class WrapArrayShallow 
{
public:
	void display(); //prints the array
	void change(); //changes the array
	//default contructor
	WrapArrayShallow() 
	{
		pca = new char[5];
		pca[0] = 'a';
		pca[1] = 'b';
		pca[2] = 'c';
		pca[3] = 'd';
		pca[4] = 'e';
	}
	//copy constructor
	WrapArrayShallow(const WrapArrayShallow& was) 
	{
		pca = was.pca;
	}
	//destructor
	~WrapArrayShallow() 
	{
		cout << "calling destructor for WrapArrayShallow\n";
		delete[] pca;
	}
private:
	char* pca;
};


int main()
{
	//Part 1
	cout << "Part 1: i = 7, pi a pointer to i, and ppi a pointer to pi.\n";

	int i = 7;
	int* pi;
	pi = &i;
	cout << "\npi " << pi << "\n";
	cout << "address of i " << &i << "\n";
	cout << "address of pi " << &pi << "\n";
	cout << "dereference of pi " << *pi << "\n";

	int** ppi;
	ppi = &pi;
	cout << "\nppi " << ppi << "\n";
	cout << "address of ppi " << &ppi << "\n";
	cout << "dereference of ppi " << *ppi << "\n";
	cout << "double dereference of ppi " << **ppi << "\n";

	//Part 2
	//Deep section
	cout << "\nPart 2: Instantiates a wrapper class for a dynamic array of 5 elements.\n";

	WrapArrayDeep wad1, *wad2;
	cout << "WrapArrayDeep 1:\n";
	wad1.display();

	wad2 = new WrapArrayDeep(wad1);
	cout << "WrapArrayDeep 2 (created using the copy constructor on 1):\n";
	wad2->display();

	wad1.change();
	cout << "After changing the contents of WrapArrayDeep 1, 1 and 3 =\n";
	wad1.display();
	wad2->display();

	//Shallow section
	cout << "\nNow doing the same steps but with WrapArrayShallow.\n";

	WrapArrayShallow was1, *was2;
	cout << "WrapArrayShallow 1:\n";
	was1.display();

	was2 = new WrapArrayShallow(was1);
	cout << "WrapArrayShallow 2 (created using the copy constructor on 1):\n";
	was2->display();

	was1.change();
	cout << "After changing the contents of WrapArrayShallow 1, 1 and 3 =\n";
	was1.display();
	was2->display();
	cout << "\n";
}

void WrapArrayDeep::display()
{
	for (int i = 0; i < 5; i++)
	{
		cout << pch[i] << " ";
	}
	cout << "\n";
}

void WrapArrayDeep::change()
{
	*pch = 60;
	*(pch + 1) = 61;
	*(pch + 2) = 62;
	*(pch + 3) = 63;
	*(pch + 4) = 64;
}

void WrapArrayShallow::display()
{
	for (int i = 0; i < 5; i++)
	{
		cout << *(pca + i) << " ";
	}
	cout << "\n";
}

void WrapArrayShallow::change()
{
	pca[0] = '<';
	pca[1] = '=';
	pca[2] = '>';
	pca[3] = '?';
	pca[4] = '@';
}